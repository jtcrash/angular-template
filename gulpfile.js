var fs = require('fs'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    path = require('path'),
    gulp = require('gulp'),
    runSequence = require('run-sequence'),
    ngHtml2Js = require('gulp-ng-html2js'),
    ngAnnotate = require('gulp-ng-annotate'),
    mainBowerFiles = require('gulp-main-bower-files'),
    gulpFilter = require('gulp-filter'),
    concat = require('gulp-concat'),
    del = require('del'),
    map = require('map-stream'),
    sass = require('gulp-sass'),
    less = require('gulp-less'),
    stripDebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    uglifycss = require('gulp-uglifycss'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    CacheBuster = require('gulp-cachebust'),
    spawn = require('child_process').spawn,
    bower = require('gulp-bower');

// Local variables and inits
var configs = JSON.parse(fs.readFileSync('./configs.json'));
var cachebust = new CacheBuster();
var config = configs.gulp;
var isProd = process.argv[2] === 'prod';
var node;

// Lint error handler (makes things pretty)
var linter = map(function (file, cb) {
  if (!file.jshint.success) {
    file.jshint.results.forEach(function (err) {
      var e = err.error;
      if (err && e.evidence) {
        gutil.log(gutil.colors.red.bold("[LINT-ERROR] " + err.file
          + " [" + e.line + ":" + e.character + "]")
          + "\n\n\t" + gutil.colors.yellow.bold(e.reason)
          + "\n\t" + gutil.colors.yellow(e.evidence) + "\n");
      }
    });
  }
  cb(null, file);
});

// SASS error styler (makes style errors pretty)
var styleErrors = function(err) {
  gutil.log(
    gutil.colors.red.bold('[STYLE-ERROR] '
    + err.relativePath + " [" + err.line + ":" + err.column + "]"),
    "\n\n\t" + gutil.colors.yellow.bold(err.messageOriginal) + "\n"
  );
}

// Node server task
gulp.task('server', function () {
  if (node) {
    node.kill();
  }
  node = spawn('node', [config.path.server], { stdio: 'inherit' });

  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

process.on('exit', function() {
  if (node) node.kill()
});

// =============================================================================
// =======                           TASKS                               =======
// =============================================================================

// User generated styles (all CSS / SCSS in the app/ dir)
gulp.task('cleanStyles', function(cb) {
  return del([ path.join(config.path.dist, 'styles.*.js') ])
});

gulp.task('styles', ['cleanStyles'], function() {
  return gulp.src([
      path.join(config.path.app, '**/*.scss'),
      path.join(config.path.app, '**/*.css')
    ]).pipe(sass().on('error', styleErrors)).pipe(sass())
    .pipe(gulpif(isProd, uglifycss()))
    .pipe(concat('styles.css'))
    .pipe(cachebust.resources())
    .pipe(gulp.dest(config.path.dist));
});

// User genereated templates (all HTML files in the app/ dir)
gulp.task('cleanTemplates', function(cb) {
  return del([ path.join(config.path.dist, 'templates.*.js') ])
});

gulp.task('templates', ['cleanTemplates'], function() {
  return gulp.src(path.join(config.path.components, '**/*.html'))
    .pipe(ngHtml2Js({ moduleName: config.angularModuleName }))
    .pipe(concat('templates.js'))
    .pipe(gulpif(isProd, uglify()))
    .pipe(cachebust.resources())
    .pipe(gulp.dest(config.path.dist));
});

// User generated scripts (all JS int eh app/ dir)
gulp.task('lint', function() {
  return gulp.src(path.join(config.path.app, '**/*.js'))
    .pipe(jshint())
    .pipe(linter);
});

gulp.task('cleanScripts', function(cb) {
  return del([ path.join(config.path.dist, 'scripts.*.js') ])
});

gulp.task('scripts', ['cleanScripts'], function() {
  return gulp.src([
      path.join(config.path.app, config.path.appFilename),
      path.join(config.path.app, '**/*.js')
    ]).pipe(concat('scripts.js'))
    .pipe(gulpif(isProd, stripDebug()))
    .pipe(gulpif(isProd, uglify()))
    .pipe(gulpif(isProd, ngAnnotate()))
    .pipe(cachebust.resources())
    .pipe(gulp.dest(config.path.dist))
});

// Vendor scripts and styles from bower
gulp.task('vendorScripts', function() {
  return gulp.src('./bower.json')
    .pipe(mainBowerFiles())
    .pipe(gulpFilter('**/*.js', { restore: true }))
    .pipe(concat('vendor.js'))
    .pipe(gulpif(isProd, uglify()))
    .pipe(cachebust.resources())
    .pipe(gulp.dest(config.path.dist));
});

gulp.task('vendorStyles', function() {
  var sassFilter = gulpFilter('**/*.scss', { restore: true }),
      lessFilter = gulpFilter('**/*.less', { restore: true }),
      cssFilter = gulpFilter('**/*.css');
  return gulp.src('./bower.json')
    .pipe(mainBowerFiles())
    .pipe(sassFilter)
    .pipe(sass())
    .pipe(concat('vendor.css'))
    .pipe(sassFilter.restore)
    .pipe(lessFilter)
    .pipe(less())
    .pipe(concat('vendor.css'))
    .pipe(lessFilter.restore)
    .pipe(cssFilter)
    .pipe(concat('vendor.css'))
    .pipe(gulpif(isProd, uglifycss()))
    .pipe(cachebust.resources())
    .pipe(gulp.dest(config.path.dist));
});

// Move over static assets
gulp.task('assets', function() {
  return gulp.src(path.join(config.path.app, config.path.assets, '/**/*'))
    .pipe(gulp.dest(path.join(config.path.dist, config.path.assets)));
});

// Cleanup and general purpose tasks
gulp.task('clean', function(cb) {
  return del([config.path.dist], cb);
});

gulp.task('bower', function() {
  return bower({ interactive: true });
});

gulp.task('cachebust', function() {
  return gulp.src(config.path.indexPath)
    .pipe(cachebust.references())
    .pipe(gulp.dest(config.path.dist));
});

// =============================================================================
// =============================================================================
// =============================================================================

// Watchers logic - scripts, styles, and templates as well as server JS or templates/views
gulp.task('updateScripts', function(cb) { runSequence('scripts', 'cachebust', cb); });
gulp.task('updateStyles', function(cb) { runSequence('styles', 'cachebust', cb); });
gulp.task('updateTemplates', function(cb) { runSequence('templates', 'cachebust', cb); });

gulp.task('watchers', function() {
  gulp.watch(path.join(config.path.app, '**/*.js'), ['updateScripts']);
  gulp.watch(path.join(config.path.app, '**/*.{css,scss}'), ['updateStyles']);
  gulp.watch(path.join(config.path.app, '**/*.html'), ['updateTemplates']);
  gulp.watch(path.join(config.path.serverRoot, '**/*.{js,html}'), ['server']);
})

// Build pathways
gulp.task('dev', function(cb) {
  runSequence(
    'clean',
    'bower',
    [
      'styles',
      'lint',
      'scripts',
      'templates',
      'vendorScripts',
      'vendorStyles',
      'assets'
    ],
    'cachebust',
    'server',
    'watchers',
    cb
  );
});

gulp.task('prod', function(cb) {
  runSequence(
    'clean',
    'bower',
    [
      'styles',
      'lint',
      'scripts',
      'templates',
      'vendorScripts',
      'vendorStyles',
      'assets'
    ],
    'cachebust',
    cb
  );
});

gulp.task('test', ['dev']);
gulp.task('default', ['dev']);
