// General includes
var http = require('http');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var express = require('express');
var connect = require('connect');
var request = require('request');
var flash = require('connect-flash');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var passport = require('passport');
var gutil = require('gulp-util');

// Module includes
var routes = require('./routes.js');

// Environment setup and inits
var configs = JSON.parse(fs.readFileSync('./configs.json'));
var config = configs.server
var router = express.Router();
var app = express();
var ENV = process.env.NODE_ENV || 'development';

// Sessions and body parser
app.set('views', path.join(__dirname, 'views/'));
app.use(flash());
app.use(cookieParser());
app.use(cookieSession({
  secret: config.secret,
  cookie: { maxAge: 1 * 24 * 60 * 60 *1000 }, // 1 day
  keys: [config.secret]

}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Authentication
if (config.auth) {
  // login(app, config);
}
// Server logic
routes(app, router, config, ENV);
app.use(express.static(config[ENV].static)); // TODO: If serving somewhere else change this to

var runServer = function() {
  var server = http.createServer(app);
  var slisten = server.listen(config[ENV].port, config[ENV].host, function(a, b, c) {
    gutil.log(gutil.colors.cyan.bold("[NODE_SERVER] ")
      + gutil.colors.cyan(ENV + ' server is running at: '
      + config[ENV].host + ':' + config[ENV].port));
  });
};

// Setup server and run
if (process.env.NODE_ENV !== 'production') {
  runServer();
} else {
  // Start cluster
  var cluster = require('cluster');
  var numCPUs = require('os').cpus().length;

  console.log(numCPUs);

  if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
      cluster.fork();
    }

    cluster.on('exit', function (worker, code, signal) {
      console.log('worker ' + worker.process.pid + ' died');
    });
  } else {
    runServer();
  }
}

exports.app = app;
