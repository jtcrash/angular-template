var path = require('path');

module.exports = function(app, router, config, ENV) {
  app.get('/', function(req, res, next) {
    res.sendFile('index.html', { root: config[ENV].static });
  });
};
